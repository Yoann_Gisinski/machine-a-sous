'''
Machine à sous :
Ce programme utilise 2 modules :
random et pygame
pygame n'est pas installé de base !
'''
import random # import du module random pour gérer les évènements aléatoires
import pygame as pg # import du module pygame pour le rendu graphique
pg.init() # initialisation du module pygame

nombre = 1 # nombre de lancés par partie
argent = 100 # argent par défaut
cout = 1 # cout d'un lancer
running = True # variable de fonctionnement

class Place(pg.sprite.Sprite) : # classe permettant de gérer la position des images
    def __init__(self, pos_x, pos_y) : # fonction d'initialisation des positions
        super().__init__() # initialisation de la super-classe "Place"
        self.image = pg.image.load('assets/7.jpg') # chargement des images par défaut au lancement du jeu
        self.rect = self.image.get_rect() # récupération des dimensions de ces images
        self.rect.x = int(pos_x) # récupération de la largeur de ces images
        self.rect.y = int(pos_y) # récupération de la hauteur de ces images
        
    def set_image(self, image) : # fonction de changement d'image
        self.image = image # changement d'image
        
def lancement(nombre) : # fonction de lancement de partie
    for i in range(nombre) : # permet de faire un nombre de lancés voulu
        global argent # définition de la variable argent en global
        argent -= cout # paiement de la partie
        hasard1 = random.choice(slot1) # valeur aléatoire de la première roue
        hasard2 = random.choice(slot2) # valeur aléatoire de la deuxième roue
        hasard3 = random.choice(slot3) # valeur aléatoire de la troisième roue
        place_left.set_image(images_dict[hasard1]) # changement d'image de la première roue
        place_center.set_image(images_dict[hasard2]) # changement d'image de la deuxième roue
        place_right.set_image(images_dict[hasard3]) # changement d'image de la troisième roue
        
        if hasard1 == hasard2 == hasard3 : # si victoire :
            slots = hasard1 # récupération de la valeur
            gains = combis_gains_dict[slots] # calcul du gain en fonction du chiffre
            argent += gains # ajout du gain à l'argent

slot1 = ["7", "5", "1", "1", "0", "0", "0"] # première roue
slot2 = ["7", "5", "5", "1", "1", "0", "0"] # deuxième roue
slot3 = ["7", "5", "5", "1", "1", "1", "0", "0"] # troisième roue

combis_gains_dict = { # gains en fonction du chiffre
    "7" : 100, # chiffre 7
    "5" : 50, # chiffre 5
    "1" : 5, # chiffre 1
    "0" : 1 # chiffre 0
    }
images_dict = { # chargement des images des chiffres
    "7" : pg.image.load('assets/7.jpg'), # chargement de l'image du chiffre 7
    "5" : pg.image.load('assets/5.jpg'), # chargement de l'image du chiffre 5
    "1" : pg.image.load('assets/1.jpg'), # chargement de l'image du chiffre 1
    "0" : pg.image.load('assets/0.jpg') # chargement de l'image du chiffre 0
    }

pg.display.set_caption("Machine à sous") # titre de la fenêtre
pg.display.set_icon(pg.image.load('assets/7.jpg')) # icon de la fenêtre
font = pg.font.SysFont("comicsansms", 30) # police d'écriture utilisée
background = pg.image.load('assets/slot_machine.jpg') # chargement de l'image de fond
image_test = pg.image.load('assets/7.jpg') # chargement de l'image par défaut
width = background.get_width() # récupération de la largeur de la fenêtre en fonction de la taille du fond
height = background.get_height() # récupération de la hauteur de la fenêtre en fonction de la taille du fond
screen = pg.display.set_mode((width, height)) # définition de la taille de la fenêtre en fonction de la taille du fond
height_place = height / 2 - 20 # position en hauteur des images
place_x_center = width / 3 + 20 # position en largeur de l'image du centre
place_x_left = place_x_center + image_test.get_width() + 30 # position en largeur de l'image de gauche
place_x_right = place_x_center - image_test.get_width() - 30 # position en largeur de l'image de droite
places = pg.sprite.Group() # définition du groupe d'emplacements
place_left = Place(place_x_left, height_place) # définition de l'emplacement de l'image de gauche
place_center = Place(place_x_center, height_place) # définition de l'emplacement de l'image du centre
place_right = Place(place_x_right, height_place) # définition de l'emplacement de l'image de droite
places.add(place_left) # ajout du placement de l'image de gauche par défaut au groupe d'emplacements
places.add(place_center) # ajout du placement de l'image du centre par défaut au groupe d'emplacements
places.add(place_right) # ajout du placement de l'image de droite par défaut au groupe d'emplacements

while running : # boucle de jeu (tant qu'il est en fonctionnement)
    screen.blit(background, (0, 0)) # affichage du fond sur l'écran
    places.draw(screen) # affichage des chiffres sur l'écran
    screen.blit(font.render("Argent : " + str(argent) + "€", True, (0, 0, 0)), (20, 10)) # affichage de l'argent sur l'écran
    screen.blit(font.render("Play : Space (1€)", True, (0, 0, 0)),(350, 10)) # affichage du bouton pour lancer un partie
    pg.display.flip() # raffraichissement de l'écran
    
    for event in pg.event.get() : # si l'utilisateur effectue une action :
        if event.type == pg.QUIT or event.type == pg.KEYDOWN and event.key == pg.K_ESCAPE : # si il ferme ou appuie sur ECHAP
            running = False # arrête le fonctionnement du jeu
            quit() # ferme la fenêtre
        elif event.type == pg.KEYDOWN : # si il appuie sur une touche
            if event.key == pg.K_SPACE and argent >= cout : #si la touche est ESPACE et qu'il a assez d'argent pour lancer une partie :
                lancement(nombre) # lancement du nombre voulu de parties